DESCRIPTION = "Example image demonstrating how to build SWUpdate compound image"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit swupdate

SRC_URI = "\
    file://emmcsetup.lua \
    file://sw-description \
"

# images to build before building swupdate image
IMAGE_DEPENDS = "core-image-minimal-base-conserver"

# images and files that will be included in the .swu image
# SWUPDATE_IMAGES = "core-image-minimal-base-conserver"
# --> The image names changed, so let's try this
# Allow also complete entries like "image.ext4.gz" in SWUPDATE_IMAGES
SWUPDATE_IMAGES = "core-image-minimal-base-conserver-${MACHINE}.rootfs.ext4.gz"
# <-- The image names changed, so let's try this

SWUPDATE_IMAGES_FSTYPES[core-image-minimal-base-conserver] = ".ext4.gz"
