FILESEXTRAPATHS:prepend := "${THISDIR}/update-image:"

# images to build before building swupdate image
# IMAGE_DEPENDS:beagle-bone-black-conserver = "core-image-minimal-base-conserver"


# images and files that will be included in the .swu image
# SWUPDATE_IMAGES:beagle-bone-black-conserver = "core-image-minimal-base-conserver"

#SWUPDATE_IMAGES_FSTYPES[core-image-minimal-base-conserver] = ".ext4.gz"

# SWUPDATE_IMAGES = "core-image-minimal-base-conserver"
# --> The image names changed, so let's try this
# Allow also complete entries like "image.ext4.gz" in SWUPDATE_IMAGES
SWUPDATE_IMAGES = "core-image-minimal-base-conserver-${MACHINE}.rootfs.ext4.gz"
# <-- The image names changed, so let's try this

