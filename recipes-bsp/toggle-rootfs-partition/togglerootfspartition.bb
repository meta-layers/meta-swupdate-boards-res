DESCRIPTION = "script, which toggles via fw_setenv the rootfs_partition variable \
               called from the swupdate lua script \
               the rootfs_partition variable is used by u-boot to pass to root partition to the kernel command line \
               in various ways - depending on the way the kernel is started"
PRIORITY = "optional"
SECTION = "examples"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"
SRC_URI = "file://togglerootfspartition.sh"

do_install() {
        # shell script, which does the work
        install -d ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/togglerootfspartition.sh ${D}${bindir}
}
