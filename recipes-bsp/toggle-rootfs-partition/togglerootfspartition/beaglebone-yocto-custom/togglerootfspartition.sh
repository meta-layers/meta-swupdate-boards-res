#!/bin/sh
#
# SPDX-License-Identifier: GPL-2.0-only
#

if [ $(swupdate -g) == /dev/mmcblk0p2 ]; then
     echo "swupdate: rootfs is on /dev/mmcblk0p2"
     echo "u-boot: rootfs_partition: $(/usr/bin/fw_printenv rootfs_partition)"
     echo "u-boot: change rootfs to /dev/mmcblk0p3"
     /usr/bin/fw_setenv rootfs_partition /dev/mmcblk0p3
     echo "u-boot: rootfs_partition: $(/usr/bin/fw_printenv rootfs_partition)"
fi

if [ $(swupdate -g) == /dev/mmcblk0p3 ]; then
     echo "rootfs on /dev/mmcblk0p3"
     echo "u-boot: rootfs_partition: $(/usr/bin/fw_printenv rootfs_partition)"
     echo "u-boot: change rootfs to /dev/mmcblk0p2"
     /usr/bin/fw_setenv rootfs_partition /dev/mmcblk0p2
     echo "u-boot: rootfs_partition: $(/usr/bin/fw_printenv rootfs_partition)"
fi

: exit 0

