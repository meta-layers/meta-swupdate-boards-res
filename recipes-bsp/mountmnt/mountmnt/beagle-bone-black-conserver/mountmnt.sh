#!/bin/sh
#
# SPDX-License-Identifier: GPL-2.0-only
#

### BEGIN INIT INFO
# Provides:          mountmnt
# Required-Start:    
# Required-Stop: 
# Default-Start:     S
# Default-Stop:
# Short-Description: Mount all filesystems.
# Description:
### END INIT INFO

#. /etc/default/rcS

mnt_mount=$(findmnt --df | grep /mnt | awk '{print $1}') 

if [ ! -d "/mnt" ]; then
     mkdir /mnt
fi

if [ $(swupdate -g) == /dev/mmcblk0p2 ]; then
     echo "rootfs on /dev/mmcblk0p2"
     #echo "/mnt should be on /dev/mmcblk0p3"
     #echo "/mnt is on $(findmnt --df | grep /mnt | awk '{print $1}')"
     if [[ ${mnt_mount} == "/dev/mmcblk0p3" ]]; then
	  echo "/mnt is already on /dev/mmcblk0p3 that's fine"
     else
          set -x
          if [ findmnt /mnt >/dev/null 2>&1 ]; then
               umount /mnt
          fi
          mount /dev/mmcblk0p3 /mnt
          set +x
     fi
fi

if [ $(swupdate -g) == /dev/mmcblk0p3 ]; then
     echo "rootfs on /dev/mmcblk0p3"
     #echo "/mnt should be on /dev/mmcblk0p2"
     #echo "/mnt is on $(findmnt --df | grep /mnt | awk '{print $1}')"
     if [[ ${mnt_mount} == "/dev/mmcblk0p2" ]]; then
	  echo "/mnt is already on /dev/mmcblk0p2 that's fine"
     else
          set -x
          if [ findmnt /mnt >/dev/null 2>&1 ]; then
               umount /mnt
          fi
	  mount /dev/mmcblk0p2 /mnt
          set +x
     fi
fi

: exit 0

