DESCRIPTION = "start up script which conditionally mounts /mnt"
PRIORITY = "optional"
SECTION = "examples"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"
SRC_URI = "file://mountmnt.sh"

SRC_URI:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'file://mountmnt.service', 'file://mountmnt.sysv', d)}"

#do_install() {
#      install -d ${D}${sysconfdir}/init.d
#      install -m 0755 ${WORKDIR}/mountmnt.sh ${D}${sysconfdir}/init.d/mountmnt.sh
#}

do_install() {
	# shell script, which does the work
        install -d ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/mountmnt.sh ${D}${bindir}

        #  --> sysvinit
        # Only install the script if 'sysvinit' is in DISTRO_FEATURES
        # systemd would be the other choice
        if ${@bb.utils.contains('DISTRO_FEATURES','sysvinit','true','false',d)}; then
           install -D -m 0755 ${UNPACKDIR}/mountmnt.sysv ${D}${sysconfdir}/init.d/mountmnt
        fi
        #  <-- sysvinit

        # --> systemd
        # Only install the script if 'systemd' is in DISTRO_FEATURES
        # systemd
        if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd','true','false',d)}; then
           install -d ${D}${systemd_unitdir}/system
           install -m 0644 ${UNPACKDIR}/mountmnt.service ${D}${systemd_unitdir}/system/mountmnt.service
        fi
        # <-- systemd
}

# Do not forget to specify the package's files:
# FILES_${PN} += "/etc/init.d/mountmnt.sh"

# --> systemd service
# please note, that above we already copy files depeding on sysvinit/systemd
#REQUIRED_DISTRO_FEATURES= "systemd"
inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
# disable for manual testing
# e.g. on target:
# systemctl start mountmnt.service
#SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "mountmnt.service"
# <-- systemd service

# --> sysvinit scripts
inherit  update-rc.d
# please note, that above we already copy files depeding on sysvinit/systemd
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME:${PN} = "mountmnt"
# script has a runlevel of: 99
# starts in initlevels:     2 3 4 5
# stops  in initlevels: 0 1         6
INITSCRIPT_PARAMS:${PN} = "start 99 2 3 4 5 . stop 99 0 1 6 ."
# <-- sysvinit scripts
