root@consrv-plus-7:/etc# df -h
Filesystem                      Size  Used Avail Use% Mounted on
/dev/root                       398M  301M   71M  81% /
devtmpfs                        206M  4.0K  206M   1% /dev
/dev/mmcblk0p6                  3.9M   73K  3.4M   3% /etc-overlay
/etc-overlay/overlay-etc/upper  3.9M   73K  3.4M   3% /etc
tmpfs                           239M     0  239M   0% /dev/shm
tmpfs                            96M   11M   85M  12% /run
tmpfs                           4.0M     0  4.0M   0% /sys/fs/cgroup
tmpfs                           239M  8.0K  239M   1% /tmp
tmpfs                           239M   60K  239M   1% /var/volatile
/dev/mmcblk0p5                  2.0G  3.8M  1.8G   1% /data
/dev/mmcblk0p1                   14M  128K   13M   1% /uenv
/dev/mmcblk0p2                  567M  474M   50M  91% /mnt

root@consrv-plus-7:/etc# ps -ef | grep swupdate
root       599     1  0 08:36 ?        00:00:00 /usr/bin/swupdate -H beaglebone 1.0 -e stable copy1 -f /etc/swupdate.cfg -w -r /www -p 8080 -u  
root       600     1  0 08:36 ?        00:00:00 /usr/bin/swupdate-progress -r -w
root       630   599  0 08:36 ?        00:00:00 /usr/bin/swupdate -H beaglebone 1.0 -e stable copy1 -f /etc/swupdate.cfg -w -r /www -p 8080 -u  
root       631   599  0 08:36 ?        00:00:00 /usr/bin/swupdate -H beaglebone 1.0 -e stable copy1 -f /etc/swupdate.cfg -w -r /www -p 8080 -u  
root       713   595  0 08:43 ttyS0    00:00:00 grep swupdate


update no-devel to devel:

Apr  8 08:44:03 consrv-plus-7 user.info swupdate: RUN [server_start] : Sleeping for 10s until retrying... 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [network_thread] : Incoming network request: processing... 
Apr  8 08:44:06 consrv-plus-7 user.info swupdate: START Software Update started ! 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [network_initializer] : Software update started 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_file_to_tmp] : Found file 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_file_to_tmp] :  filename sw-description 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_file_to_tmp] :  size 1285 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [get_common_fields] : Version 0.1.4 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [parse_hw_compatibility] : Accepted Hw Revision : 1.0 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [_parse_images] : Found compressed Image: core-image-minimal-base-conserver-devel-beagle-bone-black-conserver.ext4.gz in device : /dev/mmcblk0p2 for handler raw 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [_parse_scripts] : Found Script: emmcsetup.lua 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [_parse_bootloader] : Bootloader var: extlinux_rootfs_partiton1 = setenv rootfs_partition /dev/mmcblk0p2 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [_parse_bootloader] : Bootloader var: boot_targets = legacy_mmc0 mmc0 nand0 pxe dhcp 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [check_hw_compatibility] : Hardware beaglebone Revision: 1.0 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [check_hw_compatibility] : Hardware compatibility verified 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_files] : Found file 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        filename emmcsetup.lua 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        size 2240 required 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_files] : Found file 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        filename core-image-minimal-base-conserver-devel-beagle-bone-black-conserver.ext4.gz 
Apr  8 08:44:06 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        size 180492492 required 
Apr  8 08:44:13 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=1 from persistent storage. 
Apr  8 08:44:13 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel get operation failed (7): 'Couldn't connect to server' 
Apr  8 08:44:13 consrv-plus-7 user.info swupdate: RUN [server_start] : Sleeping for 10s until retrying... 
Apr  8 08:44:23 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=1 from persistent storage. 
Apr  8 08:44:23 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel get operation failed (7): 'Couldn't connect to server' 
Apr  8 08:44:23 consrv-plus-7 user.info swupdate: RUN [server_start] : Sleeping for 10s until retrying... 
Apr  8 08:44:33 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=1 from persistent storage. 
Apr  8 08:44:33 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel get operation failed (7): 'Couldn't connect to server' 
Apr  8 08:44:33 consrv-plus-7 user.info swupdate: RUN [server_start] : Sleeping for 10s until retrying... 
Apr  8 08:44:39 consrv-plus-7 user.debug swupdate: RUN [extract_padding] : Expecting 184 padding bytes at end-of-file 
Apr  8 08:44:39 consrv-plus-7 user.debug swupdate: RUN [network_initializer] : Valid image found: copying to FLASH 
Apr  8 08:44:39 consrv-plus-7 user.info swupdate: RUN Installation in progress 
Apr  8 08:44:39 consrv-plus-7 user.debug swupdate: RUN [start_lua_script] : Calling Lua /tmp/scripts/emmcsetup.lua 
Apr  8 08:44:39 consrv-plus-7 user.debug swupdate: RUN [run_lua_script] : LUA Exit: is boolean 1 
Apr  8 08:44:39 consrv-plus-7 user.debug swupdate: RUN [install_single_image] : Found installer for stream core-image-minimal-base-conserver-devel-beagle-bone-black-conserver.ext4.gz raw 
Apr  8 08:44:43 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=7 from persistent storage. 
Apr  8 08:44:43 consrv-plus-7 user.debug swupdate: RUN [start_suricatta] : Server initialized, entering suricatta main loop. 
Apr  8 08:46:16 consrv-plus-7 user.debug swupdate: RUN [start_lua_script] : Calling Lua /tmp/scripts/emmcsetup.lua 
Apr  8 08:46:17 consrv-plus-7 user.debug swupdate: RUN [run_lua_script] : LUA Exit: is boolean 1 
Apr  8 08:46:17 consrv-plus-7 user.debug swupdate: RUN [run_lua_script] : Script output: Post install script called script end 
Apr  8 08:46:17 consrv-plus-7 user.info swupdate: SUCCESS SWUPDATE successful ! 
Apr  8 08:46:17 consrv-plus-7 user.debug swupdate: RUN [network_initializer] : Main thread sleep again ! 
Apr  8 08:46:17 consrv-plus-7 user.info swupdate: IDLE Waiting for requests... 

--- stuck? ----

no reset

root@consrv-plus-7:/etc# df -h
Filesystem                      Size  Used Avail Use% Mounted on
/dev/root                       398M  301M   71M  81% /
devtmpfs                        206M  4.0K  206M   1% /dev
/dev/mmcblk0p6                  3.9M   73K  3.4M   3% /etc-overlay
/etc-overlay/overlay-etc/upper  3.9M   73K  3.4M   3% /etc
tmpfs                           239M     0  239M   0% /dev/shm
tmpfs                            96M   11M   85M  12% /run
tmpfs                           4.0M     0  4.0M   0% /sys/fs/cgroup
tmpfs                           239M   16K  239M   1% /tmp
tmpfs                           239M   80K  239M   1% /var/volatile
/dev/mmcblk0p5                  2.0G  3.8M  1.8G   1% /data
/dev/mmcblk0p1                   14M  128K   13M   1% /uenv
/dev/mmcblk0p2                  567M  474M   50M  91% /mnt



!!!! manual reboot !!!!

-----

-devel to non-devel


root@consrv-plus-7:~# which gdb
/usr/bin/gdb

Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [check_hw_compatibility] : Hardware compatibility verified 
Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [extract_files] : Found file 
Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        filename emmcsetup.lua 
Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        size 2414 required 
Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [extract_files] : Found file 
Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        filename core-image-minimal-base-conserver-beagle-bone-black-conserver.ext4.gz 
Apr  8 08:51:34 consrv-plus-7 user.debug swupdate: RUN [extract_files] :        size 123076822 required 
Apr  8 08:51:43 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=1 from persistent storage. 
Apr  8 08:51:43 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel get operation failed (7): 'Couldn't connect to server' 
Apr  8 08:51:43 consrv-plus-7 user.info swupdate: RUN [server_start] : Sleeping for 10s until retrying... 
Apr  8 08:51:53 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=1 from persistent storage. 
Apr  8 08:51:53 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel get operation failed (7): 'Couldn't connect to server' 
Apr  8 08:51:53 consrv-plus-7 user.info swupdate: RUN [server_start] : Sleeping for 10s until retrying... 
Apr  8 08:51:58 consrv-plus-7 user.debug swupdate: RUN [extract_padding] : Expecting 172 padding bytes at end-of-file 
Apr  8 08:51:58 consrv-plus-7 user.debug swupdate: RUN [network_initializer] : Valid image found: copying to FLASH 
Apr  8 08:51:58 consrv-plus-7 user.info swupdate: RUN Installation in progress 
Apr  8 08:51:58 consrv-plus-7 user.debug swupdate: RUN [start_lua_script] : Calling Lua /tmp/scripts/emmcsetup.lua 
Apr  8 08:51:58 consrv-plus-7 user.debug swupdate: RUN [run_lua_script] : LUA Exit: is boolean 1 
Apr  8 08:51:58 consrv-plus-7 user.debug swupdate: RUN [install_single_image] : Found installer for stream core-image-minimal-base-conserver-beagle-bone-black-conserver.ext4.gz raw 
Apr  8 08:52:03 consrv-plus-7 user.debug swupdate: RUN [do_get_state] : Read state=7 from persistent storage. 
Apr  8 08:52:03 consrv-plus-7 user.debug swupdate: RUN [start_suricatta] : Server initialized, entering suricatta main loop. 
Apr  8 08:52:03 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel get operation failed (7): 'Couldn't connect to server' 
Apr  8 08:52:03 consrv-plus-7 user.debug swupdate: RUN [server_send_target_data] : KEYVALUE= "board": "consrv-1" board consrv-1 
Apr  8 08:52:03 consrv-plus-7 user.debug swupdate: RUN [server_send_target_data] : CONFIGDATA= "board": "consrv-1" 
Apr  8 08:52:03 consrv-plus-7 user.debug swupdate: RUN [server_send_target_data] : URL=http://192.168.42.102:8080/default/controller/v1/bbb/configData JSON={ "id": "", "time": "20230408T085203", "status": { "result": { "finished": "success" }, "execution": "closed", "details" 
Apr  8 08:52:03 consrv-plus-7 user.err swupdate: FAILURE ERROR : Channel put operation failed (7): 'Couldn't connect to server' 
Apr  8 08:53:05 consrv-plus-7 user.debug swupdate: RUN [start_lua_script] : Calling Lua /tmp/scripts/emmcsetup.lua 
Apr  8 08:53:06 consrv-plus-7 user.debug swupdate: RUN [run_lua_script] : LUA Exit: is boolean 1 
Apr  8 08:53:06 consrv-plus-7 user.debug swupdate: RUN [run_lua_script] : Script output: Post install script called script end 
Apr  8 08:53:06 consrv-plus-7 user.info swupdate: SUCCESS SWUPDATE successful ! 
Apr  8 08:53:06 consrv-plus-7 user.debug swupdate: RUN [network_initializer] : Main thread sleep again ! 
Apr  8 08:53:06 consrv-plus-7 user.info swupdate: IDLE Waiting for requests... 
[  168.164312] musb-hdrc musb-hdrc.1: ep12 RX three-strikes error
[  168.174943] reboot: Restarting system

U-Boot SPL 2023.01 (Jan 09 2023 - 16:07:33 +0000)
Trying to boot from MMC1


U-Boot 2023.01 (Jan 09 2023 - 16:07:33 +0000)

CPU  : AM335X-GP rev 2.1
Model: TI AM335x BeagleBone Black
DRAM:  512 MiB
Core:  160 devices, 18 uclasses, devicetree: separate
WDT:   Started wdt@44e35000 with servicing every 1000ms (60s timeout)
NAND:  0 MiB
MMC:   OMAP SD/MMC: 0, OMAP SD/MMC: 1
Loading Environment from FAT... OK


????

============================

a hack in the lua file fixes it - no idea what exactly is the problem.
Maybe it's an issue with swupdate-progress bein started before/after swupdate?

============================

