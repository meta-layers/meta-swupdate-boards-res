# e.g. in local.conf:
# --> swupdate
# UENV_ON_PERSISTENT = "1"
# UENV_ON_PERSISTENT_FSTYPE = "vfat"
# DEVICE_FOR_UENV = "/dev/mmcblk0p1"
# MOUNT_FOR_UENV = "/boot"
# <-- swupdate

do_install:append () {
    # in local.conf:
    #    UENV_ON_PERSISTENT = "1"

    if [[ ${UENV_ON_PERSISTENT} = *1* ]]
    then
    cat >> ${D}${sysconfdir}/fstab <<EOF

# e.g. with swupdate (but not only)
# we mount ${MOUNT_FOR_UENV} on ${DEVICE_FOR_UENV} of type ${UENV_ON_PERSISTENT_FSTYPE}
# which resides e.g. on the SD card
${DEVICE_FOR_UENV}  ${MOUNT_FOR_UENV} ${UENV_ON_PERSISTENT_FSTYPE}   defaults        0       0
EOF
   fi # UENV_ON_PERSISTENT

}

